/***
 * @Author: 码上talk|RC
 * @Date: 2020-10-30 11:32:27
 * @LastEditTime: 2020-11-25 15:51:18
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-springboot/tacomall-api/tacomall-api-portal/src/main/java/store/tacomall/apiportal/strategy/impl/PageCheckoutStrategy.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package store.tacomall.apiportal.strategy.impl;

import java.util.List;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import store.tacomall.common.vo.ResponseVo;
import store.tacomall.common.util.ExceptionUtil;
import store.tacomall.entity.cart.Cart;
import store.tacomall.entity.goods.GoodsItem;
import store.tacomall.apiportal.strategy.PageStrategy;
import store.tacomall.apiportal.service.CartService;
import store.tacomall.apiportal.service.GoodsItemService;

@Component("checkout")
public class PageCheckoutStrategy implements PageStrategy {

    @Autowired
    private CartService cartService;

    @Autowired
    private GoodsItemService goodsItemService;

    @Override
    public ResponseVo<Map<String, Object>> buildPage(JSONObject json) {
        ResponseVo<Map<String, Object>> responseVo = new ResponseVo<>();
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> buys = new ArrayList<>();
        Map<String, Object> amount = new HashMap<>();
        BigDecimal totalAmount = BigDecimal.ZERO;
        String formType = json.getString("fromType");
        if (formType.equals("CART")) {
            List<Cart> cart = cartService.getCart(json).getData();
            for (int i = 0; i < cart.size(); i++) {
                Cart currentCart = cart.get(i);
                Map<String, Object> buy = new HashMap<>();
                buy.put("quantity", currentCart.getQuantity());
                buy.put("goodsItem", currentCart.getGoodsItem());
                buys.add(buy);
                totalAmount = totalAmount.add(
                        currentCart.getGoodsItem().getAmount().multiply(new BigDecimal(currentCart.getQuantity())));
            }
        } else if (formType.equals("GOODS_ITEM")) {
            GoodsItem goodsItems = goodsItemService.getGoodsItem(json).getData();
            int quantity = json.getInteger("quantity");
            Map<String, Object> buy = new HashMap<>();
            buy.put("quantity", quantity);
            buy.put("goodsItem", goodsItems);
            buys.add(buy);
            totalAmount = goodsItems.getAmount().multiply(new BigDecimal(quantity));
        } else if (formType.equals("SECKILL")) {

        } else {
            ExceptionUtil.throwClientException("非法参数：formType");
        }
        amount.put("totalAmount", totalAmount);
        map.put("buys", buys);
        map.put("amount", amount);
        responseVo.setData(map);
        return responseVo;
    }
}
