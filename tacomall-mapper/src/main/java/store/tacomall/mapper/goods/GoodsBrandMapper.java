/***
 * @Author: 码上talk|RC
 * @Date: 2020-11-14 11:18:09
 * @LastEditTime: 2020-11-14 11:18:49
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-springboot/tacomall-mapper/src/main/java/store/tacomall/mapper/goods/GoodsBrandMapper.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package store.tacomall.mapper.goods;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import store.tacomall.entity.goods.GoodsBrand;

@Repository
public interface GoodsBrandMapper extends BaseMapper<GoodsBrand> {

}
